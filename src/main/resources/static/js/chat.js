
var stompClient = null;

function connectionAuChat() {
    /*
     Je me connect au serveur via ws ;
     url definie dans WSCOnfig : stompEndpointRegistry.addEndpoint("/jacademie").withSockJS();
     */
    var socket = new SockJS('/jacademie');
    stompClient = Stomp.over(socket);

    //stompClient.connect(param, functionOnSuccess);
    stompClient.connect({}, function(message) {
        console.log('Connected : je vais ecouter /chat');

        //Le serveur va envoyé des message sur @SendTo("/chat")

        //stompClient.subscribe('ouJecoute', functionOnMessage);
        stompClient.subscribe('/chat', function(ceQueJerecoit) {
            console.log('J\'ai recu un message sur /chat de type' , ceQueJerecoit.command,
                'contenant', JSON.parse(ceQueJerecoit.body),ceQueJerecoit.body);
            //Chat retourne un objet de type ChatDTO de forme
            /*
                {
                    "date":"",
                    "from":"",
                    "message":"Hello sqd"
                }
             */

            //Je recuper le pointeur du dom de ma liste
            var monPointeurUl = document.getElementById('messageID');

            //Je cree un element de type li
            var li = document.createElement("li");
            //Je definie le contenu de mon li
            li.innerHTML = JSON.parse(ceQueJerecoit.body).message;
            //J'ajoute mon li a ma liste
            monPointeurUl.appendChild(li);
        })

        //stompClient.subscribe('ouJecoute', functionOnMessage);
        stompClient.subscribe('/chat/time', function(ceQueJerecoit) {
            console.log('J\'ai recu un message sur /chat/time de type' , ceQueJerecoit.command,
                'contenant', JSON.parse(ceQueJerecoit.body));
            //Chat/time retourne un String

            //Je recuper le pointeur du dom de ma balise h1 avec l'id dateTime
            var monPointeurUl = document.getElementById('dateTime');

            monPointeurUl.innerHTML = JSON.parse(ceQueJerecoit.body);
        })
    });

}

function sendMessageSurHello(messageAEnvoye) {
    //stompClient.send('ouJeJenvois', functionOnMessage);
    stompClient.send('/app/hello',{}, JSON.stringify(messageAEnvoye));
}

function sendMessageMonMessageInput() {
    //Je recupere ma balise input pour envoyer mon message
    var monChampForm = document.getElementById("monMessageInput");

    var valeurAEnvoyer = monChampForm.value;
    console.log(monChampForm, valeurAEnvoyer);
    sendMessageSurHello(valeurAEnvoyer);
}
