package com.sqli.jacad.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by edenayrolles on 05/02/2016.
 */
@Configuration
@EnableScheduling
public class SchedulConfig {
}
