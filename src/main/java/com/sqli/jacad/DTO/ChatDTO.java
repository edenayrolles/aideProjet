package com.sqli.jacad.DTO;

/**
 * Created by edenayrolles on 05/02/2016.
 */
public class ChatDTO {

    private String date;
    private String from;
    private String message;

    public ChatDTO(String date, String from, String message) {
        this.date = date;
        this.from = from;
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
