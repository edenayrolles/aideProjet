package com.sqli.jacad.DTO;

/**
 * Created by edenayrolles on 05/02/2016.
 */
public class Greeting {
    private String content;

    public Greeting(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}

