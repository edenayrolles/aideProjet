package com.sqli.jacad.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by edenayrolles on 05/02/2016.
 */
@Controller
public class HomeController {

    @RequestMapping("/home")
    public String getHome() {
        return "home";
    }

    @RequestMapping(value = "/helloName", method = RequestMethod.GET)
    public  String helloWorldWithName(@RequestParam("name") String nameInCtrl, Model model) {

        model.addAttribute("nameAttr", nameInCtrl);

        Collection<String> collection = new ArrayList<String>();
        collection.add("Coucou");
        collection.add("Hello");

        model.addAttribute("myCollection", collection);

        return "hello-name";
    }

}
