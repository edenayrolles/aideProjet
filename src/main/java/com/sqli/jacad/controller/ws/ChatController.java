package com.sqli.jacad.controller.ws;

import com.sqli.jacad.DTO.ChatDTO;
import com.sqli.jacad.DTO.Greeting;
import com.sqli.jacad.DTO.HelloMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;

import java.util.Date;

/**
 * Created by edenayrolles on 05/02/2016.
 */
@Controller
public class ChatController {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    /*
    Avec la config de WSConfig:
    config.setApplicationDestinationPrefixes("/app");

    @MessageMapping("/hello") ecoute sur /app/hello
     */
    @MessageMapping("/hello")
    /*
    On envois la reponse sur /chat

        //Topic ouvert
        config.enableSimpleBroker("/chat");
     */
    @SendTo("/chat")
    public ChatDTO sayHello(String  helloMessage) {
        return new ChatDTO("","","Hello " + helloMessage);
    }



/*
    @SubscribeMapping("/chat")
    public void hoockSubscribe() {
        simpMessagingTemplate.convertAndSend("/chat", new ChatDTO("","","1 guy "));
    }*/

}
