package com.sqli.jacad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by edenayrolles on 05/02/2016.
 */
@SpringBootApplication
@ComponentScan("com.sqli.jacad")
public class Start {

    public static void main(String[] args) {
        SpringApplication.run(Start.class, args);
    }

}
